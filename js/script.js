//api data section

var apiKey = '5fa65518013422bbce1aa4582357c231';
var photosetId = '72157626579923453';
var flickrURL = 'https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key='+apiKey+'&photoset_id='+photosetId+'&format=json&nojsoncallback=1';
var content = document.getElementById('content');

function get(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      reject(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}

function getJSON(url) {
    return get(url).then(JSON.parse).catch(function(err) {
    console.log("getJSON failed for", url, err);
    throw err;
  });
}

/* *******************************

Promise best used like this

******************************* */ 

// getJSON(flickrURL).then(function(response) {
//     console.log(response);
//     var sequence = Promise.resolve();
// 	return response.photoset.photo.map(function(obj) {
// 		console.log(obj);
// 		try{
// 			var length = response.photoset.photo.length;
// 		}catch(e){
// 			var mainButton = document.getElementById('main-button');
// 			var mainButtonStyle = document.querySelector('.btn-action');
// 			mainButton.disabled = true;
// 			mainButtonStyle.classList.add('disabled');
// 			mainButton.innerHTML = response.message;
// 			console.log('Error', response.message);
// 		}
// 		var img = document.createElement('img');
// 		var div = document.createElement('div');
// 		var node = document.createTextNode(obj.title);
// 		var p = document.createElement('p');
// 		p.appendChild(node);
// 		p.setAttribute('class', 'img-title');
// 		img.setAttribute('src', 'https://farm'+obj.farm+'.staticflickr.com/'+obj.server+'/'+obj.id+'_'+obj.secret+'.jpg');
// 		img.setAttribute('class', 'images');
// 		div.setAttribute('class', 'img-container');
// 		div.appendChild(img);
// 		div.appendChild(p);
// 		content.appendChild(div);
// 		sequence = sequence.then(function() {
// 			return getJSON(flickrURL);
// 		});
// 		try{
// 			var startIndex = content.firstChild;
// 			startIndex.classList.add('active');
// 		} catch(e){
// 			true;
// 		}
// 	}, Promise.resolve());
// }).catch(function(err) {
//   // Catch any error that happened along the way
//   console.log("Argh, broken: " + err.message);
// });


/* *******************************

using promise to fetch all promises at once -- could be improved again

******************************* */ 

getJSON(flickrURL).then(function(response) {
    console.log("Success!", response);
      // Take an array of promises and wait on them all
		return Promise.all(
			response.photoset.photo.map( function (obj){
				console.log(obj);
				try{
					var length = response.photoset.photo.length;
				}catch(e){
					var mainButton = document.getElementById('main-button');
					var mainButtonStyle = document.querySelector('.btn-action');
					mainButton.disabled = true;
					mainButtonStyle.classList.add('disabled');
					mainButton.innerHTML = response.message;
					console.log('Error', response.message);
				}
				var img = document.createElement('img');
				var div = document.createElement('div');
				var node = document.createTextNode(obj.title);
				var p = document.createElement('p');
				p.appendChild(node);
				p.setAttribute('class', 'img-title');
				img.setAttribute('src', 'https://farm'+obj.farm+'.staticflickr.com/'+obj.server+'/'+obj.id+'_'+obj.secret+'.jpg');
				img.setAttribute('class', 'images');
				div.setAttribute('class', 'img-container');
				div.appendChild(img);
				div.appendChild(p);
				content.appendChild(div);
				try{
					var startIndex = content.firstChild;
					startIndex.classList.add('active');
				} catch(e){
					true;
				}
			})
	  	);
	})
.catch(function(err) {
  // Catch any error that happened along the way
  console.log("Argh, broken: " + err.message);
});

/* *******************************

using promise using forEach, it is done sequentially -- could be improved

******************************* */ 

// getJSON(flickrURL).then(function(response) {
//     console.log("Success!", response);
//     var sequence = Promise.resolve();
// 	response.photoset.photo.forEach(function(obj) {
// 		console.log(obj);
// 		try{
// 			var length = response.photoset.photo.length;
// 		}catch(e){
// 			var mainButton = document.getElementById('main-button');
// 			var mainButtonStyle = document.querySelector('.btn-action');
// 			mainButton.disabled = true;
// 			mainButtonStyle.classList.add('disabled');
// 			mainButton.innerHTML = response.message;
// 			console.log('Error', response.message);
// 		}
// 		var img = document.createElement('img');
// 		var div = document.createElement('div');
// 		var node = document.createTextNode(obj.title);
// 		var p = document.createElement('p');
// 		p.appendChild(node);
// 		p.setAttribute('class', 'img-title');
// 		img.setAttribute('src', 'https://farm'+obj.farm+'.staticflickr.com/'+obj.server+'/'+obj.id+'_'+obj.secret+'.jpg');
// 		img.setAttribute('class', 'images');
// 		div.setAttribute('class', 'img-container');
// 		div.appendChild(img);
// 		div.appendChild(p);
// 		content.appendChild(div);
// 		sequence = sequence.then(function() {
// 			return getJSON(flickrURL);
// 		}).then(function(obj) {
// 		});
// 		try{
// 			var startIndex = content.firstChild;
// 			startIndex.classList.add('active');
// 		} catch(e){
// 			true;
// 		}
// 	});
// }).catch(function(err) {
//   // Catch any error that happened along the way
//   console.log("Argh, broken: " + err.message);
// });


/* *******************************

Does not use promise methodology, uses for loop and iterated through the response object

******************************* */ 

// getJSON(flickrURL).then(function(response) {
//     console.log("Success!", response);
//     try{
// 		var length = response.photoset.photo.length;
// 	}catch(e){
// 		var mainButton = document.getElementById('main-button');
// 		var mainButtonStyle = document.querySelector('.btn-action');
// 		mainButton.disabled = true;
// 		mainButtonStyle.classList.add('disabled');
// 		mainButton.innerHTML = response.message;
// 		console.log('Error', response.message);
// 	}
// 	for(var i = 0; i < length; i++){
// 		var farmId = response.photoset.photo[i].farm;
// 		var serverId = response.photoset.photo[i].server;
// 		var id = response.photoset.photo[i].id;
// 		var secretId = response.photoset.photo[i].secret;
// 		var title = response.photoset.photo[i].title;
// 		var img = document.createElement('img');
// 		var div = document.createElement('div');
// 		var node = document.createTextNode(title);
// 		var p = document.createElement('p');
// 		p.appendChild(node);
// 		p.setAttribute('class', 'img-title');
// 		img.setAttribute('src', 'https://farm'+farmId+'.staticflickr.com/'+serverId+'/'+id+'_'+secretId+'.jpg');
// 		img.setAttribute('class', 'images');
// 		div.setAttribute('class', 'img-container');
// 		div.appendChild(img);
// 		div.appendChild(p);
// 		content.appendChild(div);
// 	}
// 	try{
// 		var startIndex = content.firstChild;
// 		startIndex.classList.add('active');
// 	} catch(e){
// 		true;
// 	}
// }).catch(function(err) {
//   // Catch any error that happened along the way
//   console.log("Argh, broken: " + err.message);
// });


//next button method
var next = function(){
	var current = document.querySelector('.img-container.active');
	var next = current.nextSibling;
	if(next === null){
		var firstIndex = content.firstChild;
		firstIndex.classList.add('active');
		current.classList.remove('active');
	} else {
		next.classList.add('active');
		current.classList.remove('active');
	}
};

//previous button method
var prev = function(){
	var current = document.querySelector('.img-container.active');
	var prev = current.previousSibling;
	if(prev === null){
		var lastIndex = content.lastChild;
		lastIndex.classList.add('active');
		current.classList.remove('active');
	} else {
		prev.classList.add('active');
		current.classList.remove('active');
	}
};

//open light box method
var openLightBox = function(){
	var showContent = document.querySelector('.content-container');
	var showLightBox = document.querySelector('.lightboxOverlay');
	showContent.classList.add('active');
	showLightBox.classList.add('active');
};

//close light box method
var closeLightBox = function(){
	var closeContent = document.querySelector('.content-container.active');
	var closeLightBox = document.querySelector('.lightboxOverlay.active');
	closeContent.classList.remove('active');
	closeLightBox.classList.remove('active');
};